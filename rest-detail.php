<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Nature Healthy Food</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li> 
                                    <li class="breadcrumb-item"><a href="restaurant-list.php">Restaurants</a></li>                                  
                                    <li class="breadcrumb-item active">Nature Healthy Food</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body" data-spy="scroll" data-target="#filters" data-offset="200">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">                       
                        <!-- col -->
                       <div class="col-lg-3 leftfilter" data-spy="affix">
                           <div class="filterdiv">
                                <figure class="delvicon">
                                    <img src="img/FrezkoTaco1.png">
                                </figure>         
                                <div class="titlelist">
                                    <h3><span class="icon-spoon"></span> Options</h3>
                                </div>
                                <ul class="filters" id="filters">
                                    <li>
                                        <label class="form-container pl-0"><a href="#popularitems">Popular Items</a></label>
                                    </li>
                                    <li>
                                        <label class="form-container pl-0"><a href="#burgers">Burgers</a></label>
                                    </li>
                                    <li>
                                        <label class="form-container pl-0"><a href="#nonbeefburgers">Non Beef Burgers</a></label>
                                    </li>
                                    <li>
                                        <label class="form-container pl-0"><a href="#signature">Signature Dishes</a></label>
                                    </li>
                                                                 
                                </ul>
                            </div>
                       </div>
                        <!--/ col -->
                        <!-- col 6-->
                        <div class="col-lg-6" >

                            <!-- popular items -->
                            <div id="popularitems" class="section-food">
                                <div class="row pb-2">
                                    <div class="col-lg-12">
                                        <div class="titlelist">
                                            <h3> Popular Items</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes01.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes02.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes03.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 
                            </div>
                             <!-- popular items -->

                              <!--Burgers -->
                            <div id="burgers" class="section-food">
                                <div class="row pb-2">
                                    <div class="col-lg-12">
                                        <div class="titlelist">
                                            <h3> Burgers</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes01.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes02.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes03.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 
                            </div>
                             <!-- Burgers -->

                               <!--Non beef Burgers -->
                            <div id="nonbeefburgers" class="section-food">
                                <div class="row pb-2">
                                    <div class="col-lg-12">
                                        <div class="titlelist">
                                            <h3>Non Beef Burgers</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes01.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes02.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes03.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 
                            </div>
                             <!-- non beef Burgers -->

                               <!--signature dishes -->
                            <div id="signature" class="section-food">
                                <div class="row pb-2">
                                    <div class="col-lg-12">
                                        <div class="titlelist">
                                            <h3> Signatures</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes01.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes02.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 

                                <!--/ food col -->
                                <div class="rest-col detail-food-col">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <a href="rest-detail.php"><img src="img/data/topdishes03.jpg" class="img-fluid foodimg"></a>
                                        </div>
                                        <div class="col-lg-7 align-self-center">                                                   
                                            <h5><a href="rest-detail.php" data-toggle="modal" data-target="#addfood">Chicken Tandoori Special Food</a></h5>
                                            <p class="typefood">Cheese, tomatoes and italian herbs.</p>                                                   
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <p class="view d-flex justify-content-between">
                                                <span class="align-self-center">Rs: 250</span>
                                                <a class="addfood" href="javascript:void(0)"><span class="icon-plus"></span></a>                                                         
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/ food col --> 
                            </div>
                             <!-- signature -->

                        </div>
                        <!--/ col 6-->

                        <!-- col -->
                        <div class="col-lg-3 detcol-rt">
                            <div class="det-right">
                              <!-- delivery details -->
                                <div class="delivery-details">
                                  <table class="table deliverytable">
                                        <tr>
                                            <td><span class="icon-location-arrow"></span></td>
                                            <td>Delivery Address: 91, Plot No:345, apartname, Banjarahills, Hyderabad </td>
                                        </tr>
                                        <tr>
                                            <td><span class="icon-calendar"></span></td>
                                            <td>Delivery Date: <strong>12-05-2017</strong></td>
                                        </tr>
                                        <tr>
                                            <td><span class="icon-clock-o"></span></td>
                                            <td>Delivery Time: <strong>10 AM</strong></td>
                                        </tr>  
                                  </table>
                                </div>
                                <!--/ delivery details -->
                            <div class="titlelist">
                                <h3><span class="icon-shopping-cart"></span> &nbsp; Cart Items</h3>                                
                            </div>
                            <div class="cart-food">    
                                                
                                <table class="table">                                   
                                    <tr>
                                        <td>Chilli Burger Half Pounder Meal</td>
                                        <td>Rs:220</td>
                                        <td><a href="javascript:void(0)"><span class="icon-close"></span></a></td>
                                    </tr>
                                    <tr>
                                        <td>Subtotal</td>
                                        <td colspan="2">Rs:220</td>                                        
                                    </tr>
                                    <tr>
                                        <td>Delivery Charges</td>
                                        <td colspan="2">Rs:120</td>                                        
                                    </tr>
                                    <tr>
                                        <td>Vat</td>
                                        <td colspan="2">13%</td>                                        
                                    </tr>
                                    <tfoot>
                                        <tr>
                                            <td>Grand Total</td>
                                            <td colspan="2">Rs:500</td>                                        
                                        </tr>
                                    </tfoot>                                   
                                </table>
                                <a href="checkout-deliver-address.php" class="greenlink d-block text-center">Checkout</a>
                            </div>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->      
    </main>
    <!--/ main -->    
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

    
<!-- popup modal for variations in food or additional food -->
<!-- Modal -->
<div class="modal left fade" id="addfood" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Chicken Tandoori Special Food</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">

        <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <h4 class="choicetitle">Meat Choice</h4>
            </div>            
        </div>
        <!--/ row -->
        <form class="form-food">
            <!-- row -->       
            <div class="row py-2">
                <!-- col -->
                <div class="col-lg-6 coladd">
                    <label class="control control--radio">Barbacoa
                        <input type="radio" name="radio" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <small class="pricetag">+ Rs:250</small>                    
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-lg-6 coladd">
                    <label class="control control--radio">Carnitas
                        <input type="radio" name="radio" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <small class="pricetag">+ Rs:250</small>                    
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-lg-6 coladd">
                    <label class="control control--radio">Carne Asada
                        <input type="radio" name="radio" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <small class="pricetag">+ Rs:250</small>                    
                </div>
                <!--/ col -->

                  <!-- col -->
                <div class="col-lg-6 coladd">
                    <label class="control control--radio">Pollo Asada
                        <input type="radio" name="radio" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <small class="pricetag">+ Rs:250</small>                    
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-lg-6 coladd">
                    <label class="control control--radio">Vegetariano (No Rice or Beans)
                        <input type="radio" name="radio" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <small class="pricetag">+ Rs:250</small>                    
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-6 coladd">
                    <label class="control control--radio">Barbacoa
                        <input type="radio" name="radio" checked="checked"/>
                        <div class="control__indicator"></div>
                    </label>
                    <small class="pricetag">+ Rs:250</small>                    
                </div>
                <!--/ col -->
                
            </div>
            <!--/ row -->

        <!-- row -->
        <div class="row">
            <div class="col-lg-12">
                <h4 class="choicetitle">Add Soft Drinks</h4>
            </div>            
        </div>
        <!--/ row -->
        <!-- row -->
        <div class="row py-2">
            <!-- col -->
            <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Cilantro
                    <input type="checkbox" checked="checked"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Onion
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Pico de Gallo
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Sour Cream
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Queso
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Queso Frezko
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Lettuce
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Grilled Onions
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Grilled Peppers
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Bacon
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <!--/ col -->

        </div>
        <!--/ row -->

         <!-- row -->
         <div class="row">
            <div class="col-lg-12">
                <h4 class="choicetitle">Sides & Desserts</h4>
            </div>            
        </div>
        <!--/ row -->
        <!-- row -->
        <div class="row py-2">
            <!-- col -->
            <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Small Charro Beans
                    <input type="checkbox" checked="checked"/>
                    <div class="control__indicator"></div>                    
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Large Charro Beans
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Chips and Guacamole
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                    
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Chips and Queso
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Chips and Salsa
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">4oz Queso
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">4oz Guacamole
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Small Charro Beans
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Large Charro Beans
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6 coladd">
                <label class="control control--checkbox">Chips and Guacamole
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>                   
                </label>
                <small class="pricetag">+ Rs:250</small> 
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->
</body>

<?php include 'includes/footerscripts.php' ?>

</html>