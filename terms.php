<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Terms &amp; Conditions</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                    <li class="breadcrumb-item active">Terms & conditions</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body content-page">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                         <!-- col -->
                         <div class="col-lg-12">
                            
                            <h5 class="h5 py-2">Terms of Use</h5>
                             <p>Thank you for visiting this website, which is owned and operated by Foodsby, Inc. (“Foodsby,” “we,” “us” and “our”). These Terms of Use govern your use of the publicly available portions of this website (this “Site”). The password-protected portions of this website are intended to be used only by our clients and are governed by separate contractual agreements.</p>
                             <p>PLEASE READ THE TERMS OF USE BELOW. THESE TERMS OF USE, INCLUDING ANY REVISED AGREEMENTS THAT WE MAY POST FROM TIME TO TIME, STATE THE TERMS AND CONDITIONS UNDER WHICH FOODSBY PROVIDES YOU WITH VARIOUS SERVICES ON THE FOLLOWING THE SITE. ALL SERVICES PROVIDED BY FOODSBY ON THE SITE ARE COLLECTIVELY REFERRED TO AS “FOODSBY SERVICES.” BY ACCESSING, BROWSING AND/OR USING OUR WEB SITE AND/OR SERVICES POSTED ON THE WEB SITE, YOU ARE DEEMED TO ACCEPT THE TERMS OF USE AND AGREE TO BE BOUND BY THIS AGREEMENT WITH RESPECT TO THE USE OF THAT WEB SITE. IF YOU DO NOT WISH TO BE BOUND BY THIS AGREEMENT, PLEASE DO NOT ACCESS, BROWSE OR USE OUR WEB SITE OR ANY SERVICES WE PROVIDE.</p>
                             
                             <h5 class="h5 py-2">Ownership and Protection of Intellectual Property Rights</h5>
                             <p>The contents of our Site are intended for the personal, noncommercial use of our users. All right, title and interest to the content displayed on our Site, including but not limited to the Site’s look and feel, data, information, text, graphics, images, sound or video materials, photographs, designs, trademarks, service marks, trade names, URLs and content provided by third parties, are the property of Foodsby, or the respective third parties, and are protected by copyright, trademark, patent or other proprietary rights and laws.</p>
                             <p>Except as expressly authorized by Foodsby, you agree not to copy, modify, rent, lease, loan, sell, assign, distribute, perform, display, license, reverse engineer or create derivative works based on the Site or any Content (including without limitation any software) available through the Site.</p>
                             
                             <h5 class="h5 py-2">Changes to This Agreement</h5>
                             <p>Foodsby may make changes to this Agreement from time to time in its sole discretion. Each time changes are made to this Agreement, a revised Agreement will be posted on the home page. Your continued use of our Site following the posting of changes constitutes your acceptance of any such changes. You can review the most current version of this Agreement here. Please check this page from time to time for current terms of use.</p>

                             <h5 class="h5 py-2">Your Acceptance of Our Privacy Policy</h5>
                             <p>By agreeing to these Terms of Use, you agree to the terms of our Privacy Policy https://www.foodsby.com/main/privacy-policy/ which is expressly incorporated herein. Before using this Site, please carefully review our Privacy Policy. All information provided to us as a result of your use of this Site will be handled in accordance with our Privacy Policy. To the extent there are inconsistencies between these Terms of Use and our Privacy Policy, these Terms of Use control.</p>

                             <h5 class="h5 py-2">Other Policies</h5>
                             <p>Your use of our Site is also subject to the other policies, disclaimers and guidelines we post on such Site from time to time.</p>

                             <h5 class="h5 py-2">License Grant to Access Content on Our Site</h5>
                             <p>You are hereby granted a personal, nonexclusive, nontransferable, revocable, limited license to view, reproduce, print, cache, store and distribute content retrieved from our Site via a generally available consumer web browser, provided that you do not (and do not allow any third party to) copy, modify, create a derivative work of, reverse engineer, reverse assemble or otherwise attempt to discover any source code, sell, assign, sublicense, grant a security interest in or otherwise transfer any right in the Foodsby Services or remove or obscure the copyright notice or other notices displayed on the content. You may not reproduce, print, cache, store or distribute content retrieved from the Site in any way, for any commercial use without the prior written permission of Foodsby or the copyright holder identified in the relevant copyright notice.</p>
                             <p>Except as expressly provided in this Agreement, nothing contained in this Agreement or on the Site shall be construed as conferring any other license or right, expressly, by implication, by estoppel or otherwise, with respect to any of Foodsby’s content or under any third party’s content. Any rights not expressly granted herein are reserved.</p>
                             <p>If you choose to forward content from the Site via email through our Services, you agree that you will only forward such content to willing recipients known to you and that you will not use such Services for engaging in spam or other unauthorized conduct.</p>

                             <h5 class="h5 py-2">Your Obligations</h5>
                             <p>In consideration of your use of this Site, you agree that to the extent you provide personal information to Foodsby it will be true, accurate, current, and complete and that you will update all personal information as necessary.</p>
                             <p>To the extent you create an account through this Site, you understand and agree that any account you create, including your username and password, are personal to you and may not be used by anyone else. You are responsible for maintaining the confidentiality of your username and password and are fully responsible for all activities that occur under your username and password by you or by anyone else using your username and password, whether or not authorized by you. You agree to change your password immediately if you believe your password may have been compromised or used without authorization. You also agree to immediately inform us of any apparent breaches of security such as loss, theft or unauthorized disclosure or use of your username or password by contacting us using the information provided below. Until we are so notified you will remain liable for any unauthorized use of your account.</p>
                             <p>You agree to use this Site in a manner consistent with any and all applicable rules and regulations. You agree not to upload or transmit through this Site any computer viruses, trojan horses, worms or anything else designed to interfere with, interrupt or disrupt the normal operating procedures of a computer. Any unauthorized modification, tampering or change of any information, or any interference with the availability of or access to this Site is strictly prohibited. We reserve all rights and remedies available to us.</p>


                         </div>
                         <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>
<?php include 'includes/footerscripts.php' ?>
</html>