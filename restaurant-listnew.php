<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Restaurant Name</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item active">Restaurants</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->

            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left tab col -->
                        <div class="col-lg-9 rest-list-tab">
                            <!-- tab starts -->
                            <div class="parentVerticalTab">
                                <figure class="delvicon">
                                    <img src="img/FrezkoTaco1.png">
                                </figure>         
                                <ul class="resp-tabs-list hor_1">
                                    <li>Today</li>
                                    <li>Wednesday</li>
                                    <li>Thursday</li>
                                    <li>Friday</li>
                                    <li>Saturday</li>
                                    <li>Sunday</li>
                                </ul>
                                <div class="resp-tabs-container hor_1">
                                    <!-- sunday -->
                                    <div>

                                         <!-- restaurant col -->
                                        <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest01.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest01.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Celebrity Cafe & Bakery</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                          <!-- restaurant col -->
                                          <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Wu's Asian Bistro</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                          <!-- restaurant col -->
                                          <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest04.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Dickey's Barbecue Pit</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                          <!-- restaurant col -->
                                          <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest05.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Bread Zeppelin</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->
                                    </div>
                                    <!--/ sunday -->

                                    <!-- wed-->
                                    <div>                                        
                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest02.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest01.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest04.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->
                                    </div>
                                    <!--/ wed -->

                                    <!-- thursday -->
                                    <div>                                       
                                        <!-- restaurant col -->
                                        <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest04.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                        <!-- restaurant col -->
                                        <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                        <!-- restaurant col -->
                                        <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest02.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->
                                    </div>
                                    <!--/ thursday -->

                                    <!-- friday -->
                                    <div> 
                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->
                                    </div>
                                    <!--/ friday -->

                                    <!-- sat -->
                                    <div> 
                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest04.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Celebrity Cafe & Bakery</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Celebrity Cafe & Bakery</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->
                                        
                                    </div>
                                    <!--/ sat -->

                                    <!-- sun-->
                                    <div>
                                          <!-- restaurant col -->
                                          <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest02.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Celebrity Cafe & Bakery</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Celebrity Cafe & Bakery</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->

                                         <!-- restaurant col -->
                                         <div class="rest-col">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/rest01.jpg" class="img-fluid"></a>
                                                </div>
                                                <div class="col-lg-6"> 
                                                    <h5><a href="rest-detail.php">Celebrity Cafe & Bakery</a></h5>
                                                    <p >Delivery Time: <span class="typefood"> 10:00 AM</span></p>
                                                    <p >Order By: <span class="typefood"> 12:30 PM</span></p>
                                                    <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                                </div>
                                                <div class="col-lg-3 align-self-center">
                                                    <p class="view d-flex">
                                                        <a href="login.php"><span class="icon-heart"></span></a>
                                                        <a class="viewmenu mx-2" href="rest-detail.php">Menu</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ resturant col -->
                                    </div>
                                    <!--/ sunday-->
                                </div>
                            </div>
                            <!--/ tab ends -->
                        </div>
                        <!--/ left tab col -->

                        <!-- right col -->
                        <div class="col-lg-3">
                            <!-- delivery details -->
                            <div class="delivery-details">     
                                <!-- <figure class="delvicon">
                                    <img src="img/FrezkoTaco1.png">
                                </figure>  -->
                                <table class="table deliverytable">
                                    <tr>
                                        <td><span class="icon-location-arrow"></span></td>
                                        <td>Delivery Address: 91, Plot No:345, apartname, Banjarahills, Hyderabad </td>
                                    </tr>
                                    <tr>
                                        <td><span class="icon-clock-o"></span></td>
                                        <td>Delivery Time: <strong>10 AM</strong></td>
                                    </tr>                                       
                                    <tr>
                                        <td colspan="2">
                                            <!-- calendar -->
                                            <input class="datepicker" width="100%" placeholder="Select Deliver Date" />                                                
                                            <!--/ calendar -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="icon-calendar"></span></td>
                                        <td>Delivery Date: <strong>12-05-2017</strong></td>
                                    </tr>
                                    
                                </table>
                            </div>
                            <!--/ delivery details -->
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->      
    </main>
    <!--/ main -->

    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

</body>

<?php include 'includes/footerscripts.php' ?>
</html>