<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Secure Checkout</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                    <li class="breadcrumb-item active">Delivery Address</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body content-page">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                         <!-- col -->
                         <div class="col-lg-8">
                            <h6 class="h6 pb-2">Choose Delivery Address</h6>
                            <!-- row -->
                            <div class="row">
                                <!-- address col -->
                                <div class="col-lg-6">
                                    <div class="border p-3 address-column">
                                        <div class="row">
                                            <div class="col-lg-2 text-center">
                                                <span class="icon-home icomoon"></span>
                                            </div>
                                            <div class="col-lg-10">
                                                <h6 class="h6">Home</h6>
                                                <p>Plot No 91, Madhavaram Nagar Colony, Kukatpally, Hyderabad, Telangana 500072, India</p>
                                                <p class="fbold">46 MIN</p>
                                                                                         
                                                <a class="greenlink mb-2 d-inline-block" href="checkout-payment.php">DELIVERY HERE</a>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ address col -->

                                 <!-- address col -->
                                 <div class="col-lg-6">
                                    <div class="border p-3 address-column">
                                        <div class="row">
                                            <div class="col-lg-2 text-center">
                                              <span class="icon-suitcase icomoon"></span>
                                            </div>
                                            <div class="col-lg-10">
                                                <h6 class="h6">Office</h6>
                                                <p>Plot No 91, Madhavaram Nagar Colony, Kukatpally, Hyderabad, Telangana 500072, India</p>
                                                <p class="fbold">46 MIN</p>
                                                                                         
                                                <a class="greenlink mb-2 d-inline-block" href="checkout-payment.php">DELIVERY HERE</a>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ address col -->

                                 <!-- address col -->
                                 <div class="col-lg-6">
                                    <div class="border p-3 address-column">
                                        <div class="row">
                                            <div class="col-lg-2 text-center">
                                                <span class="icon-location-arrow icomoon"></span>
                                            </div>
                                            <div class="col-lg-10">
                                                <h6 class="h6">Add New Address</h6>
                                                <p>Add New Address to Deliver Your food </p> 
                                                                                         
                                                <a class="greenlink mb-2 d-inline-block" href="javascript:void(0)" data-target="#checkout-newaddress" data-toggle="modal"><span class="icon-plus"></span> ADD NEW ADDRESS</a> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ address col -->
                            </div>
                            <!-- row -->
                         </div>
                         <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                         <h6 class="h6 pb-2">Choosen Restaurants</h6>
                            <div class="p-3 border">
                                <div class="checkout-rest">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <img src="img/data/topdishes02.jpg" alt="" class="img-fluid">
                                        </div>
                                        <div class="col-lg-9">
                                            <h6 class="h6 pb-0 mb-0">Kritunga Restaurant</h6>
                                            <p class="pb-0"><small>Kukatpally, Hyderabad</small></p>
                                        </div>
                                        <div class="col-lg-12">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td>Veg Biryani</td>
                                                    <td>Qty
                                                        <select style="width:50px;">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Rs:225
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Chicken Biryani</td>
                                                    <td>Qty
                                                        <select style="width:50px;">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Rs:225
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Chicken Biryani</td>
                                                    <td>Qty
                                                        <select style="width:50px;">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Rs:225
                                                    </td>
                                                </tr>
                                            </table>

                                            <h6 class="h6 pl-2">Bill Details</h6>

                                            <table class="table table-borderless total-table">
                                                <tr>
                                                    <td>Item Total</td>
                                                    <td align="right">Rs: 340</td>
                                                </tr>
                                                <tr>
                                                    <td>Restaurant Charges</td>
                                                    <td align="right">Rs: 75</td>
                                                </tr>
                                                <tr>
                                                    <td>Delivery Fee</td>
                                                    <td align="right">Rs: 25</td>
                                                </tr>
                                                <tr class="border-top">
                                                    <td><h6 class="h6 txtgreen fbold">TO PAY</h6></td>
                                                    <td align="right "><h6 class="h6 txtgreen fbold">Rs:400</h6></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <!--/ col -->

                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

    
    <!-- Modal -->
<div class="modal right fade" id="checkout-newaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">
       <!-- form -->
       <form>
          <div class="form-group">
              <label>Address Line 1</label>
              <input type="text" placeholder="Enter House No and Street No" class="form-control">
          </div>          
          
          <div class="form-group">
              <label>Address Line 2</label>
              <input type="text" placeholder="Name of On Card" class="form-control">
          </div>

          <div class="form-group">
              <label>Country</label>
              <select class="form-control">
                  <option>Country</option>
                  <option>India</option>
                  <option>Srilanka</option>
              </select>
          </div>

          <div class="form-group">
              <label>State</label>
              <select class="form-control">
                  <option>State</option>
                  <option>Telangana</option>
                  <option>Andhra Pradesh</option>
              </select>
          </div>

          <div class="form-group">
              <label>City</label>
              <select class="form-control">
                  <option>City</option>
                  <option>Hyderabad</option>
                  <option>Warangal</option>
              </select>
          </div> 
          
          <div class="form-group">
              <label>Type of Office</label>
              <select class="form-control">
                  <option>Office 2</option>
                  <option>Home 2</option>
                  <option>Others </option>
              </select>
          </div>
      </form>
      <!--/ form -->
      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save Address</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->


</body>
<?php include 'includes/footerscripts.php' ?>
</html>