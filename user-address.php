<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header-postlogin.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">User Name  Will be herer</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item"><a href="user-profile.php">User Name will be here</a></li> 
                                    <li class="breadcrumb-item active">Manage Address</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3">
                           <?php include 'includes/user-navigation.php' ?>
                        </div>
                        <!--/ col -->
                        <!-- right col -->
                        <div class="col-lg-9">
                            <!-- .right profile -->
                            <div class="right-profile">
                                <h4 class="h4 border-bottom">Manage Address</h4> 
                                <!-- row -->
                                <div class="row">
                                <div class="col-lg-12 text-right pb-3">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#addnewAddress" class="txtblack"><span class="icon-plus icomoon"></span> Add New Address</a>
                                    </div>
                                    <!-- address col -->
                                    <div class="col-lg-6">
                                        <div class="border p-3 address-column">
                                            <div class="row">
                                                <div class="col-lg-2 text-center">
                                                    <span class="icon-home icomoon"></span>
                                                </div>
                                                <div class="col-lg-10">
                                                    <h6 class="h6">Home</h6>
                                                    <p>Plot No 91, Madhavaram Nagar Colony, Kukatpally, Hyderabad, Telangana 500072, India</p>

                                                    <p class="editadd d-flex">
                                                        <a data-toggle="modal" data-target="#editAddress" href="javascript:void(0)">EDIT</a>
                                                        <a href="javascript:void(0)">DELETE</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ address col -->
                                    
                                     <!-- address col -->
                                     <div class="col-lg-6">
                                        <div class="border p-3 address-column">
                                            <div class="row">
                                                <div class="col-lg-2 text-center">
                                                    <span class="icon-suitcase icomoon"></span>
                                                </div>
                                                <div class="col-lg-10">
                                                    <h6 class="h6">Office</h6>
                                                    <p>Plot No 25, 8-3-833/1/A/8, Arora Colony Rd, Sagar Society, Sri Nagar Colony, Aurora Colony, Banjara Hills, Hyderabad, Telangana 500073, India</p>

                                                    <p class="editadd d-flex">
                                                        <a href="javascript:void(0)">EDIT</a>
                                                        <a href="javascript:void(0)">DELETE</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ address col -->

                                     <!-- address col -->
                                     <div class="col-lg-6">
                                        <div class="border p-3 address-column">
                                            <div class="row">
                                                <div class="col-lg-2 text-center">
                                                    <span class="icon-location-arrow icomoon"></span>
                                                </div>
                                                <div class="col-lg-10">
                                                    <h6 class="h6">Other</h6>
                                                    <p>Plot No 25, 8-3-833/1/A/8, Arora Colony Rd, Sagar Society, Sri Nagar Colony, Aurora Colony, Banjara Hills, Hyderabad, Telangana 500073, India</p>

                                                    <p class="editadd d-flex">
                                                        <a href="javascript:void(0)">EDIT</a>
                                                        <a href="javascript:void(0)">DELETE</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ address col -->

                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right profile -->
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row-->                    
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

    
    <!-- Modal -->
<div class="modal left fade" id="addnewAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">
      <!-- form -->
      <form>
          <div class="form-group">
              <label>Address Line 1</label>
              <input type="text" placeholder="Enter House No and Street No" class="form-control">
          </div>          
          
          <div class="form-group">
              <label>Address Line 2</label>
              <input type="text" placeholder="Name of On Card" class="form-control">
          </div>

          <div class="form-group">
              <label>Country</label>
              <select class="form-control">
                  <option>Country</option>
                  <option>India</option>
                  <option>Srilanka</option>
              </select>
          </div>

          <div class="form-group">
              <label>State</label>
              <select class="form-control">
                  <option>State</option>
                  <option>Telangana</option>
                  <option>Andhra Pradesh</option>
              </select>
          </div>

          <div class="form-group">
              <label>City</label>
              <select class="form-control">
                  <option>City</option>
                  <option>Hyderabad</option>
                  <option>Warangal</option>
              </select>
          </div> 
          
          <div class="form-group">
              <label>Type of Office</label>
              <select class="form-control">
                  <option>Office 2</option>
                  <option>Home 2</option>
                  <option>Others </option>
              </select>
          </div>
      </form>
      <!--/ form -->
      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save Address</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->

   
    <!-- Modal -->
    <div class="modal right fade" id="editAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">
      <!-- form -->
      <form>
          <div class="form-group">
              <label>Address Line 1</label>
              <input type="text" placeholder="Plot No:91, Madhavaram Nagar Colony" class="form-control">
          </div>          
          
          <div class="form-group">
              <label>Address Line 2</label>
              <input type="text" placeholder="Kamala Prasanna Nagar, Kukatpally" class="form-control">
          </div>

          <div class="form-group">
              <label>Country</label>
              <select class="form-control">
                  <option>Country</option>
                  <option>India</option>
                  <option>Srilanka</option>
              </select>
          </div>

          <div class="form-group">
              <label>State</label>
              <select class="form-control">
                  <option>State</option>
                  <option>Telangana</option>
                  <option>Andhra Pradesh</option>
              </select>
          </div>

          <div class="form-group">
              <label>City</label>
              <select class="form-control">
                  <option>City</option>
                  <option>Hyderabad</option>
                  <option>Warangal</option>
              </select>
          </div> 
          
          <div class="form-group">
              <label>Type of Office</label>
              <select class="form-control">
                  <option>Office 2</option>
                  <option>Home 2</option>
                  <option>Others </option>
              </select>
          </div>
      </form>
      <!--/ form -->
      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save Address</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->


</body>

<?php include 'includes/footerscripts.php' ?>


</html>