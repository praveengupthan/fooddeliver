<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">About us</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item active">About us</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <img src="img/abt1-600x454.jpg" alt="" class="img-fluid">
                            <h5 class="h5 py-2">Taste Driven</h5>
                            <p>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat.</p>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-4">
                            <img src="img/ab2-600x454.jpg" alt="" class="img-fluid">
                            <h5 class="h5 py-2">Made the Quality Way</h5>
                            <p>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum.</p>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-4">
                            <img src="img/ab23-600x454.jpg" alt="" class="img-fluid">
                            <h5 class="h5 py-2">Family of Creators</h5>
                            <p>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
                <div class="container-fluid pu-4 my-4">
                    <img src="img/about.jpg" class="img-fluid" alt="">
                </div>
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <h5 class="h5 py-2">About us</h5>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-4">
                           <p>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat.</p>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-4">
                           <p>Mauris tempus erat laoreet turpis lobortis, eu tincidunt erat fermentum. Aliquam non tincidunt urna. Integer tincidunt nec nisl vitae ullamcorper. Proin sed ultrices erat.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>

<?php include 'includes/footerscripts.php' ?>


</html>