<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Login</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item active">Forgot Password</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">                           
                            <form class="form py-4 signform">
                                <div class="form-group">
                                    <label>Username or Email</label>
                                    <input type="text" class="form-control" placeholder="Username">
                                </div>                              
                                
                                <div class="form-group">
                                    <input type="submit" class="greenlink" value="Reset Password">
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                        <a href="login.php">Login with Username</a>                                    
                                    </div>
                                <p class="py-3">Once You Click on Reset Password You will Get Activation Link to your Registered Email</p>
                                
                            </form>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>

<?php include 'includes/footerscripts.php' ?>


</html>