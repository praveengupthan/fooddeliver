<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Secure Payment</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                    <li class="breadcrumb-item active">Payment</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                         <!-- col -->
                         <div class="col-lg-8">                           
                            <!-- row -->
                            <div class="row">
                                <!-- address col -->
                                <div class="col-lg-12">
                                    <div class="border p-3 address-column">
                                        <div class="row">                                            
                                            <div class="col-lg-9">
                                                 <h5 class="h5 pb-2">Delivery Address</h5>
                                                <h6 class="h6">Home</h6>
                                                <p>Plot No 91, Madhavaram Nagar Colony, Kukatpally, Hyderabad, Telangana 500072, India</p>
                                                <p class="fbold">46 MIN</p>         
                                            </div>
                                            <div class="col-lg-3 text-right">
                                                <a class="fbold txtgreen" href="checkout-deliver-address.php">CHANGE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ address col -->                                
                            </div>
                            <!-- row -->
                            <!--payment method col --> 
                            <div class="row">
                                    <div class="col-lg-12">
                                        <div class="border p-3 overflow-hidden">
                                            <h5 class="h5 pb-2">Choose payment method</h5>

                                            <!-- payment method tab starts -->
                                            <div class="parentVerticalTab custom-left-tab">
                                                <ul class="resp-tabs-list hor_1">
                                                    <li>Wallets</li>
                                                    <li>Credit / Debit / ATM Card</li>
                                                    <li>Net Banking</li>
                                                    <li>Pay On Delivery</li>
                                                </ul>
                                                <div class="resp-tabs-container hor_1">
                                                    <!-- wallets -->
                                                    <div> 
                                                        <div class="border p-4 d-flex justify-content-between">
                                                            <figure>
                                                                <img src="img/amazonpay.png" alt="">
                                                                <h6 class="h6">Amazon Pay</h6>
                                                            </figure>
                                                                <a href="javascript:void(0)" class="greenlink mt-1 d-inline-block">Pay Rs:205</a>
                                                        </div>
                                                        <div class="border p-4 d-flex justify-content-between">
                                                            <figure>
                                                                <img src="img/phonepayicon.png" alt="">
                                                                <h6 class="h6">Phone Pay</h6>
                                                            </figure>
                                                                <a href="javascript:void(0)" class="txtgreen mt-1 fbold">LINK ACCOUNT</a>
                                                        </div>
                                                        <div class="border p-4 d-flex justify-content-between">
                                                            <figure>
                                                                <img src="img/paytmicon.png" alt="">
                                                                <h6 class="h6">Paytm Pay</h6>
                                                                <p>Available Balance :Rs: 1200.00</p>
                                                            </figure>
                                                            <a href="javascript:void(0)" class="greenlink mt-1 d-inline-block">Pay Rs:205</a>
                                                        </div>
                                                        <div class="border p-4 d-flex justify-content-between">
                                                            <figure>
                                                                <img src="img/mobikwicklogo.png" alt="">
                                                                <h6 class="h6">Mobikwick</h6>
                                                                <p class="txtgray pb-3">Mobikwik is experiencing low success reates currently, please use an alternate payment method</p>
                                                            </figure>
                                                            <a href="javascript:void(0)" class="txtgreen mt-1 fbold">LINK ACCOUNT</a>
                                                        </div>
                                                        <div class="border p-4 d-flex justify-content-between">
                                                        <figure>
                                                                <img src="img/freechargelogo.png" alt="">
                                                        <h6 class="h6">Freecharge</h6>                                  </figure>                    
                                                            <a href="javascript:void(0)" class="txtgreen mt-1 fbold">LINK ACCOUNT</a>
                                                        </div>
                                                    </div>
                                                    <!--/ wallets -->

                                                    <!--Credit / Debit / ATM Card-->
                                                    <div> 
                                                        <p><span class="float-left mr-3">We Accept</span> <span><img src="img/cards.png" alt=""></span></p>
                                                        <form class="mt-3">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <input type="text" placeholder="Card Number" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <div class="row">
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <select class="form-control">
                                                                                    <option>Expiry Month</option>
                                                                                    <option>January</option>
                                                                                    <option>February</option>
                                                                                    <option>March</option>
                                                                                    <option>April</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-6">
                                                                            <div class="form-group">
                                                                                <select class="form-control">
                                                                                    <option>Expiry Year</option>
                                                                                    <option>2022</option>
                                                                                    <option>2023</option>
                                                                                    <option>2024</option>
                                                                                    <option>2025</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <input type="text" placeholder="CVV " class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <input type="text" placeholder="Name on Card" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 py-2">
                                                                    <label class="control control--checkbox txtgray">Securely save this card for a faster checkout next time
                                                                        <input type="checkbox"/>
                                                                        <div class="control__indicator"></div>
                                                                    </label>                                                                  
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <a href="javascript:void(0)" class="greenlink mt-4 d-block text-center">Pay Rs:205</a>
                                                                </div>
                                                                <div class="col-lg-12 py-3">
                                                                    <p><small class="txtgray">Card Details will be saved securely, based of the industry standard.</small></p>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!--/ Credit / Debit / ATM Card-->
                                                    <!--Net Banking-->
                                                    <div class="ml-3">                                                      
                                                       <div class="row">
                                                           <div clas="col-lg-6">
                                                               <div class="p-2 m-2 border d-flex">
                                                                   <p>HDFC</p>
                                                                   <label class="control control--radio ml-4">&nbsp;
                                                                        <input type="radio" name="radio"/>
                                                                        <div class="control__indicator"></div>
                                                                    </label>
                                                               </div>
                                                           </div>
                                                           <div clas="col-lg-6">
                                                               <div class="p-2 m-2 border d-flex">
                                                                   <p>ICICI</p>
                                                                   <label class="control control--radio ml-4">&nbsp;
                                                                        <input type="radio" name="radio"/>
                                                                        <div class="control__indicator"></div>
                                                                    </label>
                                                               </div>
                                                           </div>
                                                           <div clas="col-lg-6">
                                                               <div class="p-2 m-2 border d-flex">
                                                                   <p>AXIS</p>
                                                                   <label class="control control--radio ml-4">&nbsp;
                                                                        <input type="radio" name="radio"/>
                                                                        <div class="control__indicator"></div>
                                                                    </label>
                                                               </div>
                                                           </div>
                                                           <div clas="col-lg-6">
                                                               <div class="p-2 m-2 border d-flex">
                                                                   <p>KOTAK</p>
                                                                   <label class="control control--radio ml-4">&nbsp;
                                                                        <input type="radio" name="radio"/>
                                                                        <div class="control__indicator"></div>
                                                                    </label>
                                                               </div>
                                                           </div>
                                                           <div clas="col-lg-6">
                                                               <div class="p-2 m-2 border d-flex">
                                                                   <p>SBI</p>
                                                                   <label class="control control--radio ml-4">&nbsp;
                                                                        <input type="radio" name="radio"/>
                                                                        <div class="control__indicator"></div>
                                                                    </label>
                                                               </div>
                                                           </div>
                                                       </div>
                                                       <!-- row -->
                                                       <div class="row mt-4">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <select class="form-control">
                                                                    <option>Other Bank</option>
                                                                    <option>Panjab National Bank</option>
                                                                    <option>Andhra Bank</option>
                                                                    <option>Society Bank</option>
                                                                    <option>Telangana Bank</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                            <div class="col-lg-12">
                                                                <a href="javascript:void(0)" class="greenlink mt-1 d-block text-center">Pay Rs:205</a>
                                                            </div>
                                                       </div>
                                                       <!--/row -->
                                                    </div>
                                                    <!--/Net Banking -->
                                                    <!--Pay On Delivery -->
                                                    <div class="text-center">    
                                                        <p><img src="img/paying.svg" alt="" width="100px;"></p>                                                  
                                                        <p>Pay On Delivery</p>
                                                        <a href="javascript:void(0)" class="greenlink mt-1 d-block text-center">Pay Rs:205</a>
                                                    </div>
                                                    <!--/ Pay On Delivery-->
                                                </div>
                                            </div>
                                            <!--/ payment method tab ends -->
                                        </div>
                                    </div>
                                </div>                       
                                <!--/ payment method col -->
                             </div>
                         <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                         <h6 class="h6 pb-2">Choosen Restaurants</h6>
                            <div class="p-3 border">
                                <div class="checkout-rest">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <img src="img/data/topdishes02.jpg" alt="" class="img-fluid">
                                        </div>
                                        <div class="col-lg-9">
                                            <h6 class="h6 pb-0 mb-0">Kritunga Restaurant</h6>
                                            <p class="pb-0"><small>Kukatpally, Hyderabad</small></p>
                                        </div>
                                        <div class="col-lg-12">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td>Veg Biryani</td>
                                                    <td>Qty
                                                        <select style="width:50px;">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Rs:225
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Chicken Biryani</td>
                                                    <td>Qty
                                                        <select style="width:50px;">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Rs:225
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Chicken Biryani</td>
                                                    <td>Qty
                                                        <select style="width:50px;">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        Rs:225
                                                    </td>
                                                </tr>
                                            </table>

                                            <h6 class="h6 pl-2">Bill Details</h6>

                                            <table class="table table-borderless total-table">
                                                <tr>
                                                    <td>Item Total</td>
                                                    <td align="right">Rs: 340</td>
                                                </tr>
                                                <tr>
                                                    <td>Restaurant Charges</td>
                                                    <td align="right">Rs: 75</td>
                                                </tr>
                                                <tr>
                                                    <td>Delivery Fee</td>
                                                    <td align="right">Rs: 25</td>
                                                </tr>
                                                <tr class="border-top">
                                                    <td><h6 class="h6 txtgreen fbold">TO PAY</h6></td>
                                                    <td align="right "><h6 class="h6 txtgreen fbold">Rs:400</h6></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

    
    <!-- Modal -->
<div class="modal right fade" id="checkout-newaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">
       <!-- form -->
       <form>
          <div class="form-group">
              <label>Address Line 1</label>
              <input type="text" placeholder="Enter House No and Street No" class="form-control">
          </div>          
          
          <div class="form-group">
              <label>Address Line 2</label>
              <input type="text" placeholder="Name of On Card" class="form-control">
          </div>

          <div class="form-group">
              <label>Country</label>
              <select class="form-control">
                  <option>Country</option>
                  <option>India</option>
                  <option>Srilanka</option>
              </select>
          </div>

          <div class="form-group">
              <label>State</label>
              <select class="form-control">
                  <option>State</option>
                  <option>Telangana</option>
                  <option>Andhra Pradesh</option>
              </select>
          </div>

          <div class="form-group">
              <label>City</label>
              <select class="form-control">
                  <option>City</option>
                  <option>Hyderabad</option>
                  <option>Warangal</option>
              </select>
          </div> 
          
          <div class="form-group">
              <label>Type of Office</label>
              <select class="form-control">
                  <option>Office 2</option>
                  <option>Home 2</option>
                  <option>Others </option>
              </select>
          </div>
      </form>
      <!--/ form -->
      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save Address</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->

</body>
<?php include 'includes/footerscripts.php' ?>
</html>