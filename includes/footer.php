
 <!-- subscribe news letter -->
 <div class="subscribe">
        <!-- container -->
        <div class="container">
                <!-- row title -->
                <div class="row justify-content-center title-row">
                <div class="col-lg-8 text-center">
                    <h2>Subscribe to News Letter</h2>
                    <h6 class="py-2">Subscribe to receive our weekly Hot Promotions every Monday!</h6>

                    <div class="subscribe-in mt-4 d-flex">
                        <input type="text" placeholder="Type your email address to receive our newsletter" name="">
                        <input type="submit" value="Subcribe" name="">
                    </div>
                </div>
            </div>
            <!--/ row title -->
        </div>
        <!--/ container -->
    </div>
    <!--/ subscribe news letter -->
<footer>
        <!--container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-2">
                    <h4>Top Cities</h4>
                    <ul>
                        <li><a href="javascript:void(0)">Hyderabad</a></li>
                        <li><a href="javascript:void(0)">Secunderabad</a></li>
                        <li><a href="javascript:void(0)">Warangal</a></li>
                        <li><a href="javascript:void(0)">Nizamabad</a></li>
                        <li><a href="javascript:void(0)">Adilabad</a></li>
                        <li><a href="javascript:void(0)">Kukatpally</a></li>
                        <li><a href="javascript:void(0)">Musheerabad</a></li>
                        <li><a href="javascript:void(0)">Nampally</a></li>
                        <li><a href="javascript:void(0)">Rajendra Nagar</a></li>
                    </ul>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-2">                   
                    <ul class="pt-5">
                        <li><a href="javascript:void(0)">Kakinada</a></li>
                        <li><a href="javascript:void(0)">Vijayawada</a></li>
                        <li><a href="javascript:void(0)">Guntur</a></li>
                        <li><a href="javascript:void(0)">Tenali</a></li>
                        <li><a href="javascript:void(0)">Amaravati</a></li>
                        <li><a href="javascript:void(0)">Ameerpet</a></li>
                        <li><a href="javascript:void(0)">Panjagutta</a></li>
                        <li><a href="javascript:void(0)">Himayath Nagar</a></li>
                        <li><a href="javascript:void(0)">Dilsukh Nagar</a></li>
                    </ul>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-2">                   
                    <ul class="pt-5">
                        <li><a href="javascript:void(0)">Kakinada</a></li>
                        <li><a href="javascript:void(0)">Vijayawada</a></li>
                        <li><a href="javascript:void(0)">Guntur</a></li>
                        <li><a href="javascript:void(0)">Tenali</a></li>
                        <li><a href="javascript:void(0)">Amaravati</a></li>
                        <li><a href="javascript:void(0)">Ameerpet</a></li>
                        <li><a href="javascript:void(0)">Panjagutta</a></li>
                        <li><a href="javascript:void(0)">Himayath Nagar</a></li>
                        <li><a href="javascript:void(0)">Dilsukh Nagar</a></li>
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-2">
                    <h4>Top Cuisines near You</h4>
                    <ul>
                        <li><a href="javascript:void(0)">Pizza Near Me</a></li>
                        <li><a href="javascript:void(0)">Chines Food Near Me</a></li>
                        <li><a href="javascript:void(0)">Sushi Near Me</a></li>
                        <li><a href="javascript:void(0)">Cafe Near me</a></li>
                        <li><a href="javascript:void(0)">Mexican Food Near me</a></li>
                        <li><a href="javascript:void(0)">Thai Food Near me</a></li>
                        <li><a href="javascript:void(0)">Lunch Near Me</a></li>                        
                    </ul>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-2">                   
                    <ul class="pt-5">
                        <li><a href="javascript:void(0)">Seafood Near Me</a></li>
                        <li><a href="javascript:void(0)">Indian Food Near Me</a></li>
                        <li><a href="javascript:void(0)">Dessert near me</a></li>
                        <li><a href="javascript:void(0)">Burgers Near me</a></li>
                        <li><a href="javascript:void(0)">Asian Food Near me</a></li>
                        <li><a href="javascript:void(0)">Haleem Near me</a></li>
                        <li><a href="javascript:void(0)">Sandwich Near me</a></li>                       
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-2">                   
                    <ul class="pt-5">
                        <li><a href="javascript:void(0)">Pizza Near Me</a></li>
                        <li><a href="javascript:void(0)">Chines Food Near Me</a></li>
                        <li><a href="javascript:void(0)">Sushi Near Me</a></li>
                        <li><a href="javascript:void(0)">Cafe Near me</a></li>
                        <li><a href="javascript:void(0)">Mexican Food Near me</a></li>
                        <li><a href="javascript:void(0)">Thai Food Near me</a></li>
                        <li><a href="javascript:void(0)">Lunch Near Me</a></li>                       
                    </ul>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->

             <!-- row -->
             <div class="row">
                <!-- col -->
                <div class="col-lg-2">
                    <h4>Get to Know  us</h4>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About us</a></li>
                        <li><a href="javascript:void(0)">Careers</a></li>
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="contact.php">Contact</a></li>
                        <li><a href="javascript:void(0)">Customer Care</a></li>
                        <li><a href="javascript:void(0)">Testimonials</a></li>
                        <li><a href="javascript:void(0)">News & Interviews</a></li>                       
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-2">
                    <h4>Let us Help You</h4>
                    <ul>
                        <li><a href="javascript:void(0)">Help</a></li>
                        <li><a href="faq.php">FAQ’s</a></li>
                        <li><a href="javascript:void(0)">Support</a></li>                                   
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-2">
                    <h4>Doing Business</h4>
                    <ul>
                        <li><a href="javascript:void(0)">Become a Disher</a></li>
                        <li><a href="javascript:void(0)">Be a Partner Restaurant</a></li>
                        <li><a href="javascript:void(0)">Get Dishies for Deliveries</a></li>                                   
                    </ul>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-2">
                    <h4>Policies</h4>
                    <ul>
                        <li><a href="terms.php">Terms & Conditions</a></li>
                        <li><a href="privacy.php">Privacy Policy</a></li>
                        <li><a href="refund-policy.php">Refund Policy</a></li>
                    </ul>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-2">
                    <h4>Touch with us</h4>
                    <ul>
                        <li><a><span class="fbold">Phone:</span> +91 96256578</a></li>
                        <li><a>Email: </a></li>
                        <li><a href="mailto:info@orderfood.com">info@orderfood.com</a></li>  
                        <li><a href="mailto:support@orderfood.com">support@orderfood.com</a></li>                                 
                    </ul>
                    
                    <p class="py-2 footersocial">
                        <a class="px-1" href="javascript:void(0)" target="_blank"><span class="icon-facebook"></span></a>
                        <a class="px-1" href="javascript:void(0)" target="_blank"><span class="icon-twitter"></span></a>
                        <a class="px-1" href="javascript:void(0)" target="_blank"><span class="icon-linkedin"></span></a>
                    </p>
                   
                </div>
                <!--/ col -->
             </div>
             <!-- row -->
        </div>
        <!--/ container -->
        <!-- copy rights -->
        <div class="copyrights">
            <a id="movetop" class="" href="javascript:void(0)"><span class="icon-arrow-up icomoon"></span></a>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 text-center">
                        <p>All copy rights Reserved @ Food and Bakery 2019</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ copy rights -->
    </footer>