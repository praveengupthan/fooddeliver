<header class="fixed-top">       
        <!-- navigation -->
        <nav class="navbar navbar-expand-lg ">
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt="" title=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bars icomoon"></span>
                  </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Get the App</a>
                    </li>                   
                    <li class="nav-item dropdown postloginuser">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"> User Name will be here </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="user-profile.php">Profile</a>
                            <a class="dropdown-item" href="user-address.php">Manage Address</a>
                            <a class="dropdown-item" href="user-orders.php">My Orders</a>
                            <a class="dropdown-item" href="user-payments.php">Payments</a>
                            <a class="dropdown-item" href="index.php">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!--/ navigatin -->
    </header>