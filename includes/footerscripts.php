<!-- script files-->
<script src="js/jquery.min.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function() {           
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>
<script>
    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
</script>
<script src="js/easyResponsiveTabs.js"></script>

<script src="js/custom.js"></script>
<!--[if lt IE 9]>
<script src="js/html5-shiv.js"></script>
<![endif]-->
<script src="js/accordion.js"></script>


<script>
     $(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
  });
</script>