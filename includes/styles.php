<link rel="icon" type="image/png" sizes="16x16" href="img/fav.png">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/reset.css">
<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/fonts.css">
<link rel="stylesheet" href="css/form.css">
<link rel="stylesheet" href="css/easy-responsive-tabs.css">
<link rel="stylesheet" href="css/modal.css">
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>


