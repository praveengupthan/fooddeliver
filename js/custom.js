$(document).ready(function(){

  //hide news in header 

  $(".closeicon").click(function(){
    $(".news-updates").hide();
  });

 
    //header add class
    $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
            $(" .fixed-top ").addClass(" fixed-theme ");
        } else {
            $(" .fixed-top ").removeClass(" fixed-theme ");
        }
    });

    //restaurant detail left column fixed on scroll

    $(window).scroll(function(){
      if($(this).scrollTop() > 100 ){
        $(".filterdiv").addClass("fixeddet");
      } else{
        $(".filterdiv").removeClass("fixeddet");
      }
    });

    $(window).scroll(function(){
      if($(this).scrollTop() > 100 ){
        $(".det-right").addClass("detfix");
      } else{
        $(".det-right").removeClass("detfix");
      }
    });

    

    //onclick browser move top
    $(document).ready(function() {
        //window scroll top
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#movetop').fadeIn();
            } else {
                $('#movetop').fadeOut();
            }
        });
        //Click event to scroll to top
        $('#movetop').click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

   });

   //responsive tabs
   $(document).ready(function() {
    //Horizontal Tab
    $('.parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
    // Child Tab
    $('#ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });
    //Vertical Tab
    $('.parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});

//  script for accordiaon 
// Hiding the panel content. If JS is inactive, content will be displayed
$( '.panel-content' ).hide();

// Preparing the DOM

// -- Update the markup of accordion container 
$( '.accordion' ).attr({
  role: 'tablist',
  multiselectable: 'true'
 });

// -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
$( '.panel-content' ).attr( 'id', function( IDcount ) { 
  return 'panel-' + IDcount; 
});
$( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) { 
  return 'control-panel-' + IDcount; 
});
$( '.panel-content' ).attr( 'aria-hidden' , 'true' );
// ---- Only for accordion, add role tabpanel
$( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );

// -- Wrapping panel title content with a <a href="">
$( '.panel-title' ).each(function(i){
  
  // ---- Need to identify the target, easy it's the immediate brother
  $target = $(this).next( '.panel-content' )[0].id;
  
  // ---- Creating the link with aria and link it to the panel content
  $link = $( '<a>', {
    'href': '#' + $target,
    'aria-expanded': 'false',
    'aria-controls': $target,
    'id' : 'control-' + $target
  });
  
  // ---- Output the link
  $(this).wrapInner($link);  
  
});

// Optional : include an icon. Better in JS because without JS it have non-sense.
$( '.panel-title a' ).append('<span class="icon">+</span>');

// Now we can play with it
$( '.panel-title a' ).click(function() {
  
  if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !
    
    // -- Only for accordion effect (2 options) : comment or uncomment the one you want
    
    // ---- Option 1 : close only opened panel in the same accordion
    //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
    $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');

    // Option 2 : close all opened panels in all accordion container
    //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);
    
    // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
    $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');

  } else { // The current panel is opened and we want to close it

    $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;

  }
  // No Boing Boing
  return false;
});



//scroll spy 
$("#filters li a").on('click', function(e) {

  // prevent default anchor click behavior
  e.preventDefault();

  // store hash
  var hash = this.hash;

  // animate
  $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 1000, function(){

      // when done, add hash to url
      // (default click behaviour)
      window.location.hash = hash;
    });

});

   
  
  
  