<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">FAQ</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                    <li class="breadcrumb-item active">Privacy Policy</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body content-page">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                         <!-- col 8 -->
                         <div class="col-lg-8">
                            <h5 class="h5 py-2">Top Questions</h5>
                            <div class="accord">
                                <div class="accordion">
                                    <h3 class="panel-title">Where is my order?</h3>
                                    <div class="panel-content">
                                        <p>After you place your order, we send it directly to the restaurant, which starts preparing it immediately. Our restaurants do everything they can to get your food delivered as quickly as possible. However, sometimes restaurants receive very large amount of orders, or drivers get stuck in heavy traffic - this unfortunately might cause delays.</p>
                                        <p>If the amount of time you’ve waited has exceeded the estimated delivery time, you can contact us and we’ll look into what’s going on.</p>
                                    </div>
                                    <h3 class="panel-title">I have a voucher code. How can I use it?</h3>
                                    <div class="panel-content">
                                        <p>If you have a voucher code, you can redeem it after selecting a restaurant and adding items to your basket. You will see a field to enter your voucher code on the order overview page. If the voucher is valid, the discount on your order will be calculated immediately. Only one voucher can be used per order.</p>
                                        <p>If your voucher code does not work, feel free to contact us and we’ll take a look.</p>
                                    </div>
                                    <h3 class="panel-title">Are there any discounts available at foodpanda.in right now?</h3>
                                    <div class="panel-content">
                                        <p>foodpanda.in offers many different discounts at restaurants. If you want to view them all, have a look at: Restaurants with Discounts</p>
                                        <p>If your voucher code does not work, feel free to contact us and we’ll take a look.</p>
                                    </div>
                                    <h3 class="panel-title">Where is my order?</h3>
                                    <div class="panel-content">
                                        <p>After you place your order, we send it directly to the restaurant, which starts preparing it immediately. Our restaurants do everything they can to get your food delivered as quickly as possible. However, sometimes restaurants receive very large amount of orders, or drivers get stuck in heavy traffic - this unfortunately might cause delays.</p>
                                        <p>If the amount of time you’ve waited has exceeded the estimated delivery time, you can contact us and we’ll look into what’s going on.</p>
                                    </div>
                                    <h3 class="panel-title">I have a voucher code. How can I use it?</h3>
                                    <div class="panel-content">
                                        <p>If you have a voucher code, you can redeem it after selecting a restaurant and adding items to your basket. You will see a field to enter your voucher code on the order overview page. If the voucher is valid, the discount on your order will be calculated immediately. Only one voucher can be used per order.</p>
                                        <p>If your voucher code does not work, feel free to contact us and we’ll take a look.</p>
                                    </div>
                                    <h3 class="panel-title">Are there any discounts available at foodpanda.in right now?</h3>
                                    <div class="panel-content">
                                        <p>foodpanda.in offers many different discounts at restaurants. If you want to view them all, have a look at: Restaurants with Discounts</p>
                                        <p>If your voucher code does not work, feel free to contact us and we’ll take a look.</p>
                                    </div>
                                    <h3 class="panel-title">Where is my order?</h3>
                                    <div class="panel-content">
                                        <p>After you place your order, we send it directly to the restaurant, which starts preparing it immediately. Our restaurants do everything they can to get your food delivered as quickly as possible. However, sometimes restaurants receive very large amount of orders, or drivers get stuck in heavy traffic - this unfortunately might cause delays.</p>
                                        <p>If the amount of time you’ve waited has exceeded the estimated delivery time, you can contact us and we’ll look into what’s going on.</p>
                                    </div>
                                    <h3 class="panel-title">I have a voucher code. How can I use it?</h3>
                                    <div class="panel-content">
                                        <p>If you have a voucher code, you can redeem it after selecting a restaurant and adding items to your basket. You will see a field to enter your voucher code on the order overview page. If the voucher is valid, the discount on your order will be calculated immediately. Only one voucher can be used per order.</p>
                                        <p>If your voucher code does not work, feel free to contact us and we’ll take a look.</p>
                                    </div>
                                    <h3 class="panel-title">Are there any discounts available at foodpanda.in right now?</h3>
                                    <div class="panel-content">
                                        <p>foodpanda.in offers many different discounts at restaurants. If you want to view them all, have a look at: Restaurants with Discounts</p>
                                        <p>If your voucher code does not work, feel free to contact us and we’ll take a look.</p>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <!--/ col 8 -->

                         <!-- col 4 -->
                         <div class="col-lg-4">
                            <h5 class="h5 py-2">Contact our customer service</h5>
                            <div class="p-4 border">
                                <h5 class="h4">Food Court</h5>
                                <p class="flightgray">mon-thu: 11.00 – 23.00</p>
                                <p class="flightgray pb-3">sat-sun: 12.00 – 21.00PM</p>

                                <p><span class="icon-phone"></span> + 1 555 2367890</p>
                                <p><span class="icon-paper-plane"></span> info@fooddeliver.com</p>
                            </div>
                         </div>
                         <!--/ col 4 -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>
<?php include 'includes/footerscripts.php' ?>
</html>