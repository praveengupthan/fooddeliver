<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header-postlogin.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">User Name  Will be herer</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item active">User Name will be here</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3">
                           <?php include 'includes/user-navigation.php' ?>
                        </div>
                        <!--/ col -->
                        <!-- right col -->
                        <div class="col-lg-9">
                            <!-- .right profile -->
                            <div class="right-profile">
                                <h4 class="h4 border-bottom">My Profile</h4>
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <ul class="userlist">
                                            <li>
                                                <p>Name</p>
                                                <h4 class="h6">User Name will be here</h4>
                                            </li>

                                            <li>
                                                <p>Phone Number</p>
                                                <h4 class="h6">+91 9642123254</h4>
                                            </li>

                                            <li>
                                                <p>E-Mail</p>
                                                <h4 class="h6">praveennandipati@gmail.com</h4>
                                            </li>

                                            <li>
                                                <p>Password</p>
                                                <h4 class="h6">******</h4>
                                            </li>
                                        </ul>
                                        <a class="greenlink" href="javascript:void(0)" data-toggle="modal" data-target="#editprofile">Edit</a>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right profile -->
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row-->
                    
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

    <!-- Modal -->
<div class="modal right fade" id="editprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">

      <!-- form -->
      <form>
          <div class="form-group">
              <label>User Name</label>
              <input type="text" placeholder="Your Name will be here" class="form-control">
          </div>
          <div class="form-group">
              <label>Email</label>
              <input type="text" placeholder="Enter Email" class="form-control">
          </div>
          <div class="form-group">
              <label>Contact Number</label>
              <input type="text" placeholder="Enter Phone Number" class="form-control">
          </div>
          <div class="form-group">
              <label>Password</label>
              <input type="password" placeholder="Your Password" class="form-control">
          </div>
      </form>
      <!--/ form -->
       
       

      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->


</body>

<?php include 'includes/footerscripts.php' ?>


</html>