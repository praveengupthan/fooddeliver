<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header-postlogin.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">User Name  Will be herer</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item"><a href="user-profile.php">User Name will be here</a></li> 
                                    <li class="breadcrumb-item active">My Favourites</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3">
                           <?php include 'includes/user-navigation.php' ?>
                        </div>
                        <!--/ col -->
                        <!-- right col -->
                        <div class="col-lg-9">
                            <!-- .right profile -->
                            <div class="right-profile">
                                <h4 class="h4 border-bottom">My Favourites</h4>
                                <!-- row -->
                                <div class="row">
                                   <!-- col -->
                                   <div class="col-lg-4">
                                       <div class="favourite-col p-2 border">
                                            <figure class="position-relative">
                                                <a href="javascript:void(0)"><img src="img/data/blog/blog01.jpg" alt="" class="img-fluid"></a>
                                                <span class="position-absolute fav-food active-fav"><span class="icon-heart icomoon"></span></span>
                                            </figure>
                                            <article>
                                                <h6 class="h6"><a href="javascript:void(0)">Paradise Biryani</a></h6>
                                                <p>Biryani, India, Chinese, North Indian</p>
                                                <p class="txtgreen txt-green d-flex flex-wrap justify-content-between border-top pt-2 mt-2">
                                                    <span>Rs: 250</span>
                                                    <span>45 MIN</span>
                                                </p>
                                            </article>
                                       </div>
                                   </div>
                                   <!--/ col -->

                                   <!-- col -->
                                   <div class="col-lg-4">
                                       <div class="favourite-col p-2 border">
                                            <figure class="position-relative">
                                                <a href="javascript:void(0)"><img src="img/data/blog/blog02.jpg" alt="" class="img-fluid"></a>
                                                <span class="position-absolute fav-food active-fav"><span class="icon-heart icomoon"></span></span>
                                            </figure>
                                            <article>
                                                <h6 class="h6"><a href="javascript:void(0)">Paradise Biryani</a></h6>
                                                <p>Biryani, India, Chinese, North Indian</p>
                                                <p class="txtgreen txt-green d-flex flex-wrap justify-content-between border-top pt-2 mt-2">
                                                    <span>Rs: 250</span>
                                                    <span>45 MIN</span>
                                                </p>
                                            </article>
                                       </div>
                                   </div>
                                   <!--/ col -->

                                   <!-- col -->
                                   <div class="col-lg-4">
                                       <div class="favourite-col p-2 border">
                                            <figure class="position-relative">
                                                <a href="javascript:void(0)"><img src="img/data/blog/blog04.jpg" alt="" class="img-fluid"></a>
                                                <span class="position-absolute fav-food active-fav"><span class="icon-heart icomoon"></span></span>
                                            </figure>
                                            <article>
                                                <h6 class="h6"><a href="javascript:void(0)">Paradise Biryani</a></h6>
                                                <p>Biryani, India, Chinese, North Indian</p>
                                                <p class="txtgreen txt-green d-flex flex-wrap justify-content-between border-top pt-2 mt-2">
                                                    <span>Rs: 250</span>
                                                    <span>45 MIN</span>
                                                </p>
                                            </article>
                                       </div>
                                   </div>
                                   <!--/ col -->

                                   <!-- col -->
                                   <div class="col-lg-4">
                                       <div class="favourite-col p-2 border">
                                            <figure class="position-relative">
                                                <a href="javascript:void(0)"><img src="img/data/blog/blog05.jpg" alt="" class="img-fluid"></a>
                                                <span class="position-absolute fav-food active-fav"><span class="icon-heart icomoon"></span></span>
                                            </figure>
                                            <article>
                                                <h6 class="h6"><a href="javascript:void(0)">Paradise Biryani</a></h6>
                                                <p>Biryani, India, Chinese, North Indian</p>
                                                <p class="txtgreen txt-green d-flex flex-wrap justify-content-between border-top pt-2 mt-2">
                                                    <span>Rs: 250</span>
                                                    <span>45 MIN</span>
                                                </p>
                                            </article>
                                       </div>
                                   </div>
                                   <!--/ col -->

                                   <!-- col -->
                                   <div class="col-lg-4">
                                       <div class="favourite-col p-2 border">
                                            <figure class="position-relative">
                                                <a href="javascript:void(0)"><img src="img/data/blog/blog06.jpg" alt="" class="img-fluid"></a>
                                                <span class="position-absolute fav-food active-fav"><span class="icon-heart icomoon"></span></span>
                                            </figure>
                                            <article>
                                                <h6 class="h6"><a href="javascript:void(0)">Paradise Biryani</a></h6>
                                                <p>Biryani, India, Chinese, North Indian</p>
                                                <p class="txtgreen txt-green d-flex flex-wrap justify-content-between border-top pt-2 mt-2">
                                                    <span>Rs: 250</span>
                                                    <span>45 MIN</span>
                                                </p>
                                            </article>
                                       </div>
                                   </div>
                                   <!--/ col -->

                                   <!-- col -->
                                   <div class="col-lg-4">
                                       <div class="favourite-col p-2 border">
                                            <figure class="position-relative">
                                                <a href="javascript:void(0)"><img src="img/data/blog/blog01.jpg" alt="" class="img-fluid"></a>
                                                <span class="position-absolute fav-food active-fav"><span class="icon-heart icomoon"></span></span>
                                            </figure>
                                            <article>
                                                <h6 class="h6"><a href="javascript:void(0)">Paradise Biryani</a></h6>
                                                <p>Biryani, India, Chinese, North Indian</p>
                                                <p class="txtgreen txt-green d-flex flex-wrap justify-content-between border-top pt-2 mt-2">
                                                    <span>Rs: 250</span>
                                                    <span>45 MIN</span>
                                                </p>
                                            </article>
                                       </div>
                                   </div>
                                   <!--/ col -->


                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right profile -->
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row-->
                    
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>

<?php include 'includes/footerscripts.php' ?>


</html>