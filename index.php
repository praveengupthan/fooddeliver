<!DOCTYPE html>
<html class="no-js"  lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
    
</head>
<body >
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!--search block -->
        <div class="search-section">
            <div class="container-fluid pl-0">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- hide and show slider -->
                        <div id="carouselExampleFade" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="img-fluid" src="img/slider01.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="img-fluid" src="img/slider02.jpg" alt="">
                                </div>
                                <div class="carousel-item">
                                    <img class="img-fluid" src="img/slider03.jpg" alt="">
                                </div>
                            </div>                           
                        </div>
                        <!--/ hide and show slider -->
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-6 align-self-center search-col">
                        <div class="search-in w-75 mx-auto text-center">
                            <h6>DISCOVER OUR RESTAURANTS</h6>
                            <h1>We belive in Taste</h1>
                            <!-- search -->
                            <div class="search-columns">
                                <form class="search-form">
                                    <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-4 px-0">
                                            <div class="form-group position-relative">         
                                                <span class="icon position-absolute"><span class="icon-location-arrow"></span></span> 
                                                <select class="form-control">
                                                    <option>Select City</option>
                                                    <option>Hyderabad</option>
                                                    <option>Secunderabad</option>
                                                </select>                                                
                                            </div>
                                        </div>
                                        <!--/ col -->
                                        <!-- col -->
                                        <div class="col-lg-7 px-0">
                                            <div class="form-group position-relative">         
                                                <span class="icon position-absolute"><span class="icon-search"></span></span> 
                                               <input type="text" class="form-control" placeholder="Enter Delivery Address">                                    
                                            </div>
                                        </div>
                                        <!--/ col -->
                                        <!-- col -->
                                        <div class="col-lg-1 px-0">
                                            <input type="button" value="&nbsp;" class="btn" onclick='window.location.href="restaurant-list.php";'>
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->
                                </form>
                            </div>
                            <!--/ search -->
                        </div>
                     </div>
                     <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </div>
        <!--/ search block -->
        <!-- how it works -->
        <div class="how-itworks pt-5">
             <!--container -->
             <div class="container">
                 <!-- row title -->
                 <div class="row justify-content-center title-row">
                     <div class="col-lg-8 text-center">
                         <h2>How we get ready food for you</h2>
                         <h6 class="py-2">Food delivery or pickup from local restaurants</h6>
                     </div>
                 </div>
                 <!--/ row title -->
                 <!-- row columns -->
                 <div class="row py-5">
                     <!-- col -->
                     <div class="col-lg-3 text-center howcol">
                        <figure class="mx-auto"><img src="img/search-hotel.svg" alt="" title=""></figure>
                        <article>
                            <h4>1 Choose A Restaurant</h4>
                            <p>Cras vitae dictum velit. Duis at purus enim. Cras massa massa, maximus sit amet finibus quis, pharetra eu erat.</p>
                        </article>
                     </div>
                     <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center howcol">
                            <figure class="mx-auto"><img src="img/breakfast.svg" alt="" title=""></figure>
                            <article>
                                <h4>2 Order your Tasty Dish</h4>
                                <p>Cras vitae dictum velit. Duis at purus enim. Cras massa massa, maximus sit amet finibus quis, pharetra eu erat.</p>
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                        <div class="col-lg-3 text-center howcol">
                            <figure class="mx-auto"><img src="img/scooter.svg" alt="" title=""></figure>
                            <article>

                                <h4>3 Pay & Pick your food</h4>
                                <p>Cras vitae dictum velit. Duis at purus enim. Cras massa massa, maximus sit amet finibus quis, pharetra eu erat.</p>
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3 text-center howcol">
                            <figure class="mx-auto"><img src="img/dining.svg" alt="" title=""></figure>
                            <article>
                                
                                <h4>4  Enjoy Your Food</h4>
                                <p>Cras vitae dictum velit. Duis at purus enim. Cras massa massa, maximus sit amet finibus quis, pharetra eu erat.</p>
                            </article>
                        </div>
                        <!--/ col -->

                 </div>
                 <!--/ row columns -->
             </div>
             <!--/ container -->
        </div>
        <!--/ how it works -->

        <!-- top restaurants -->
        <div class="top-restaurants py-5">
            <!-- container -->
            <div class="container">
                <!-- row title -->
                <div class="row justify-content-center title-row">
                    <div class="col-lg-8 text-center">
                        <h2>Top Restaurants</h2>
                        <h6 class="py-2">Explore restaurants, bars, and cafés by locality</h6>
                    </div>
                </div>
                <!--/ row title -->
                <!-- row -->
                <div class="row py-4">
                    <div class="col-lg-12">
                        <ul class="justify-content-center d-flex">
                            <li><a href="javascript:void(0)"><img src="img/data/rest01.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest02.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest03.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest04.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest05.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest06.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest07.jpg" alt=""></a></li>
                            <li><a href="javascript:void(0)"><img src="img/data/rest08.jpg" alt=""></a></li>
                        </ul>
                    </div>
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ top restaurants -->

        <!-- top dishes -->
        <div class="top-dishes">
            <!-- container -->
            <div class="container">
                 <!-- row title -->
                 <div class="row justify-content-center title-row">
                    <div class="col-lg-8 text-center">
                        <h2>Top Dishes in Hyderabad</h2>
                        <h6 class="py-2">Explore restaurants by Popular Dishes in Hyderabad city</h6>
                    </div>
                </div>
                <!--/ row title -->
            </div>
            <!--/ container -->
            <!-- container fluid -->
            <div class="container-fluid no-gutters">
                <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <div class="col-lg-2 px-1">
                        <figure class="dishfigure position-relative">
                            <a href="javascript:void(0)"><img src="img/data/topdishes01.jpg" alt="" class="img-fluid"></a>
                            <span class="price position-absolute">Starts Rs: 200</span>
                        </figure>
                        <article class="dishtitle text-center">
                            <h6>Hyderabad Dub Biryani</h6>
                        </article>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-2 px-1">
                        <figure class="dishfigure position-relative">
                            <a href="javascript:void(0)"><img src="img/data/topdishes02.jpg" alt="" class="img-fluid"></a>
                            <span class="price position-absolute">Starts Rs: 75</span>
                        </figure>
                        <article class="dishtitle text-center">
                            <h6>Chicken Burger with  Cheese</h6>
                        </article>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-2 px-1">
                        <figure class="dishfigure position-relative">
                            <a href="javascript:void(0)"><img src="img/data/topdishes03.jpg" alt="" class="img-fluid"></a>
                            <span class="price position-absolute">Starts Rs: 75</span>
                        </figure>
                        <article class="dishtitle text-center">
                            <h6>Milk Shakes & Juices</h6>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-2 px-1">
                        <figure class="dishfigure position-relative">
                            <a href="javascript:void(0)"><img src="img/data/topdishes04.jpg" alt="" class="img-fluid"></a>
                            <span class="price position-absolute">Starts Rs: 75</span>
                        </figure>
                        <article class="dishtitle text-center">
                            <h6>Dosa with Coke Combo</h6>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-2 px-1">
                        <figure class="dishfigure position-relative">
                            <a href="javascript:void(0)"><img src="img/data/topdishes05.jpg" alt="" class="img-fluid"></a>
                            <span class="price position-absolute">Starts Rs: 75</span>
                        </figure>
                        <article class="dishtitle text-center">
                            <h6>Roti with Burger</h6>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-2 px-1">
                        <figure class="dishfigure position-relative">
                            <a href="javascript:void(0)"><img src="img/data/topdishes06.jpg" alt="" class="img-fluid"></a>
                            <span class="price position-absolute">Starts Rs: 75</span>
                        </figure>
                        <article class="dishtitle text-center">
                            <h6>Vegetarian Thai Roti</h6>
                        </article>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid -->
        </div>
        <!--/ top dishes -->

        <!-- testimonials -->
        <div class="testimonials-home pt-5">
            <!-- container -->
            <div class="container">
                 <!-- row title -->
                 <div class="row justify-content-center title-row">
                    <div class="col-lg-8 text-center">
                        <h2>What People Says about us</h2>
                        <h6 class="py-2">Explore Testimonials and What people Says about us</h6>
                    </div>
                </div>
                <!--/ row title -->
            </div>
            <!--/ container -->
            <!-- container -->
            <div class="container test-container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-12 d-flex testcol">
                         <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"  data-toggle="popover" title="Name will be here" data-placement="right" data-content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit adipiscing elit. adipiscing elit."><img src="img/data/test01.png" alt="" title=""></a>
                            </figure>
                            
                         </div>

                         <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test02.png" alt="" title=""></a>
                            </figure>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 d-flex testcol">
                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test03.png" alt="" title=""></a>
                            </figure>
                            </div>

                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test04.png" alt="" title=""></a>
                            </figure>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-4 d-flex testcol">
                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test05.png" alt="" title=""></a>
                            </figure>
                            </div>

                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test06.png" alt="" title=""></a>
                            </figure>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->              

                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 d-flex testcol">
                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test01.png" alt="" title=""></a>
                            </figure>
                            </div>

                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test02.png" alt="" title=""></a>
                            </figure>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-12 d-flex testcol">
                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test03.png" alt="" title=""></a>
                            </figure>
                            </div>

                            <div class="tester">
                            <figure>
                                <a href="javascript:void(0)"><img src="img/data/test04.png" alt="" title=""></a>
                            </figure>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

            </div>
            <!--/ container -->
        </div>
        <!--/ testimonials -->

       
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>

<?php include 'includes/footerscripts.php' ?>


</html>