<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Contact</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                                                  
                                    <li class="breadcrumb-item active">Contact</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-8">
                            <h5 class="h4">Make Online Enquiry</h5>
                            <p>Override the digital divide. Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.</p>
                            <form class="form-contact pt-4">

                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Your Name (required)</label>
                                        <input type="text" placeholder="Enter full name" class="form-control" required>
                                    </div>
                                </div>
                                <!-- col -->

                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Your Email (required)</label>
                                        <input type="text" placeholder="Your Email" class="form-control" required>
                                    </div>
                                </div>
                                <!-- col -->
                            </div>
                            <!--/ row -->

                             <!-- row -->
                             <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" placeholder="Subject" class="form-control" required>
                                    </div>
                                </div>
                                <!-- col -->

                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Contact Number (required)</label>
                                        <input type="text" placeholder="Contact Number" class="form-control" required>
                                    </div>
                                </div>
                                <!-- col -->
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea class="form-control" placeholder="Enter Your Mesage">
                                        </textarea>
                                    </div>
                                </div>
                                <!-- col -->                               
                            </div>
                            <!--/ row -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <input type="submit" class="btn btn-success w-100" value="Submit">
                                </div>
                            </div>

                            </form>
                        </div>
                        <!--/ col -->

                        <!-- right contact details -->
                        <div class="col-lg-3">
                            <img src="img/contact-img.png" alt="" class="img-fluid">
                            <div class="p-4 border text-center">
                                <h5 class="h4">Food Court</h5>
                                <p class="flightgray">mon-thu: 11.00 – 23.00</p>
                                <p class="flightgray pb-3">sat-sun: 12.00 – 21.00PM</p>

                                <p><span class="icon-phone"></span> + 1 555 2367890</p>
                                <p><span class="icon-paper-plane"></span> info@fooddeliver.com</p>
                            </div>
                        </div>
                        <!--/ right contact details -->                        
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <div class="col-lg-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15225.895442867586!2d78.44108889999998!3d17.43701955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1559222323416!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>
<?php include 'includes/footerscripts.php' ?>
</html>