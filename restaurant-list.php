<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">List of Restaurant</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item active">Restaurants</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">                       
                        <!-- left filters -->
                        <div class="col-lg-3">
                            <div class="titlelist">
                                <h3><span class="icon-cutlery"></span> Cuisines</h3>                                
                            </div>
                            <ul class="filters">
                                <li>
                                <label class="control control--checkbox">Beef Roast
                                    <input type="checkbox" checked="checked"/>
                                    <div class="control__indicator"></div>
                                </label>
                                </li>
                                <li>
                                    <label class="control control--checkbox">Carrot Juice 
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                     <label class="control control--checkbox">Cheese Burger
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control--checkbox">Chicken Roast
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                     <label class="control control--checkbox">Chines Soup
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control--checkbox">Cold Coffee
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control--checkbox">Open Now
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control--checkbox">Cold Coffee
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                            </ul>
                            <div class="titlelist">
                                <h3><span class="icon-clock-o"></span> Open Status</h3>                                
                            </div>
                            <ul class="filters">
                                <li>
                                    <label class="control control--checkbox">Open Now
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                                <li>
                                 <label class="control control--checkbox">Close
                                        <input type="checkbox" />
                                        <div class="control__indicator"></div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <!--/ left filters -->
                        <!-- middle -->
                        <div class="col-lg-9">
                            <h4 class="h5 pb-4">18 Restaurant's Found</h4>

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest01.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Nature Healthy Food</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest02.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Menu & Drinks</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest03.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Chefs</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest04.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Menu’s</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest05.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Food N&H</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest06.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Restaurant</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest07.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Pizza Hut</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest08.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Food And Drinks</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest01.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Talco Bell</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <!-- restaurant col -->
                            <div class="rest-col">
                               <div class="row">
                                   <div class="col-lg-2">
                                       <a href="rest-detail.php"><img src="img/data/rest02.jpg" class="img-fluid"></a>
                                   </div>
                                   <div class="col-lg-7">
                                       <p>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star rank"></span>
                                            <span class="icon-star"></span>
                                       </p>
                                       <h5><a href="rest-detail.php">Pizza Pizza</a></h5>
                                       <p class="typefood">Type of food : Apple Juice, Beef Roast, Cheese Burger</p>
                                       <p class="pt-2"><span class="icon-location-arrow"></span> Ameerpet, Hyderabad</p>
                                   </div>
                                   <div class="col-lg-3 align-self-center">
                                       <p class="view">
                                           <a href="login.php"><span class="icon-heart"></span></a>
                                           <a class="viewmenu" href="rest-detail.php">View Menu</a>
                                       </p>
                                   </div>
                               </div>
                            </div>
                            <!--/ resturant col -->

                            <ul class="pagination">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>


                        </div>
                        <!--/ middle -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>

<?php include 'includes/footerscripts.php' ?>


</html>