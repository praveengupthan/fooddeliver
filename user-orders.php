<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header-postlogin.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">User Name  Will be herer</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item"><a href="user-profile.php">User Name will be here</a></li> 
                                    <li class="breadcrumb-item active">My Orders</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3">
                           <?php include 'includes/user-navigation.php' ?>
                        </div>
                        <!--/ col -->
                        <!-- right col -->
                        <div class="col-lg-9">
                            <!-- .right profile -->
                            <div class="right-profile">
                                <h4 class="h4 border-bottom">My Orders</h4>
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <!-- order row -->
                                        <div class="order-row">
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/data/hotel01.jpg" class="img-fluid" alt=""></a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-5">
                                                    <h5 class="h6"><a class="txtgreen" href="rest-detail.php">Restairant Name</a></h5>
                                                    <p><small>KPHB Main Road Kukatpally</small></p>
                                                    <p class="pb-2"><small>Order:#0123456789</small><small>Sun May 19, 17:56 hrs</small></p>
                                                    <a class="txtgreen fbold" href="rest-detail.php">VIEW DETAILS</a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-4 text-right">
                                                    <small>Delivered on Sun, May 19, 8:23 PM</small>
                                                    <a href="javascript:void(0)" class="greenlink mt-3 float-right">Re Order</a>
                                                </div>
                                                <!--/ col -->
                                            </div>
                                            <!-- row -->
                                            <div class="p-4 border-top mt-4">
                                                <!-- row -->
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <p><span>Veg Biryani X 1</span></p></div>
                                                    <div class="col-lg-6 text-right align-self-center">
                                                        <p>Total paid: <span>Rs: 250</span></p>
                                                    </div>                                                   
                                                </div>                                                
                                                <!--/ row -->
                                            </div>
                                        </div>
                                        <!--/ order row -->

                                         <!-- order row -->
                                         <div class="order-row">
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/slider02.jpg" class="img-fluid" alt=""></a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-5">
                                                    <h5 class="h6"><a class="txtgreen" href="rest-detail.php">Restairant Name</a></h5>
                                                    <p><small>KPHB Main Road Kukatpally</small></p>
                                                    <p class="pb-2"><small>Order:#0123456789</small><small>Sun May 19, 17:56 hrs</small></p>
                                                    <a class="txtgreen fbold" href="rest-detail.php">VIEW DETAILS</a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-4 text-right">
                                                    <small>Delivered on Sun, May 19, 8:23 PM</small>
                                                    <a href="javascript:void(0)" class="greenlink mt-3 float-right">Re Order</a>
                                                </div>
                                                <!--/ col -->
                                            </div>
                                            <!-- row -->
                                            <div class="p-4 border-top mt-4">
                                                <!-- row -->
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <p><span>Veg Biryani X 1</span></p></div>
                                                    <div class="col-lg-6 text-right align-self-center">
                                                        <p>Total paid: <span>Rs: 250</span></p>
                                                    </div>                                                   
                                                </div>                                                
                                                <!--/ row -->
                                            </div>
                                        </div>
                                        <!--/ order row -->

                                         <!-- order row -->
                                         <div class="order-row">
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/slider03.jpg" class="img-fluid" alt=""></a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-5">
                                                    <h5 class="h6"><a class="txtgreen" href="rest-detail.php">Restairant Name</a></h5>
                                                    <p><small>KPHB Main Road Kukatpally</small></p>
                                                    <p class="pb-2"><small>Order:#0123456789</small><small>Sun May 19, 17:56 hrs</small></p>
                                                    <a class="txtgreen fbold" href="rest-detail.php">VIEW DETAILS</a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-4 text-right">
                                                    <small>Delivered on Sun, May 19, 8:23 PM</small>
                                                    <a href="javascript:void(0)" class="greenlink mt-3 float-right">Re Order</a>
                                                </div>
                                                <!--/ col -->
                                            </div>
                                            <!-- row -->
                                            <div class="p-4 border-top mt-4">
                                                <!-- row -->
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <p><span>Veg Biryani X 1</span></p></div>
                                                    <div class="col-lg-6 text-right align-self-center">
                                                        <p>Total paid: <span>Rs: 250</span></p>
                                                    </div>                                                   
                                                </div>                                                
                                                <!--/ row -->
                                            </div>
                                        </div>
                                        <!--/ order row -->

                                         <!-- order row -->
                                         <div class="order-row">
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-3">
                                                    <a href="rest-detail.php"><img src="img/slider01.jpg" class="img-fluid" alt=""></a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-5">
                                                    <h5 class="h6"><a class="txtgreen" href="rest-detail.php">Restairant Name</a></h5>
                                                    <p><small>KPHB Main Road Kukatpally</small></p>
                                                    <p class="pb-2"><small>Order:#0123456789</small><small>Sun May 19, 17:56 hrs</small></p>
                                                    <a class="txtgreen fbold" href="rest-detail.php">VIEW DETAILS</a>
                                                </div>
                                                <!--/ col -->
                                                <!-- col -->
                                                <div class="col-lg-4 text-right">
                                                    <small>Delivered on Sun, May 19, 8:23 PM</small>
                                                    <a href="javascript:void(0)" class="greenlink mt-3 float-right">Re Order</a>
                                                </div>
                                                <!--/ col -->
                                            </div>
                                            <!-- row -->
                                            <div class="p-4 border-top mt-4">
                                                <!-- row -->
                                                <div class="row">
                                                    <div class="col-lg-6 align-self-center">
                                                        <p><span>Veg Biryani X 1</span></p></div>
                                                    <div class="col-lg-6 text-right align-self-center">
                                                        <p>Total paid: <span>Rs: 250</span></p>
                                                    </div>                                                   
                                                </div>                                                
                                                <!--/ row -->
                                            </div>
                                        </div>
                                        <!--/ order row -->
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right profile -->
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row-->
                    
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>

<?php include 'includes/footerscripts.php' ?>


</html>