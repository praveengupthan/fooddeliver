<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header-postlogin.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">User Name  Will be herer</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>                                   
                                    <li class="breadcrumb-item"><a href="user-profile.php">User Name will be here</a></li> 
                                    <li class="breadcrumb-item active">My payments</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3">
                           <?php include 'includes/user-navigation.php' ?>
                        </div>
                        <!--/ col -->
                        <!-- right col -->
                        <div class="col-lg-9">
                            <!-- .right profile -->
                            <div class="right-profile">
                                <h4 class="h4 border-bottom">Manage Saved Cards</h4>
                                <!-- row -->
                                <div class="row"> 
                                    <div class="col-lg-12 text-right pb-3">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#addnewcard" class="txtblack"><span class="icon-plus icomoon"></span> Add New Card</a>
                                    </div>
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="p-2 border position-relative accol">
                                            <p class="pb-3">ICICI Bank Credit Card</p>
                                            <p class="d-flex flex-wrap">
                                                <span><img src="img/data/visa.svg"></span>
                                                <span class="pl-3 acno">123456*****25668</span>
                                            </p>
                                            <span class="position-absolute delicon"><a href="javascript:void(0)"><span class="icon-trash icomoon"></span></a></span>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="p-2 border position-relative accol">
                                            <p class="pb-3">State Bank of India Credit Card</p>
                                            <p class="d-flex flex-wrap">
                                                <span><img src="img/data/visa.svg"></span>
                                                <span class="pl-3 acno">123456*****25668</span>
                                            </p>
                                            <span class="position-absolute delicon"><a href="javascript:void(0)"><span class="icon-trash icomoon"></span></a></span>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->

                                <!-- row -->
                                <div class="row py-5">
                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <h5 class="h5 pb-3">FAQ's about Save Card</h5>
                                        <div class="card-faq pb-3">
                                            <h6 class="h6">Why is my Card being saved on Food Deliver?</h6>
                                            <p>It's quicker. You can save the hassle of typing in the complete card information every time you shop at Flipkart by saving your card details. You can make your payment by selecting the saved card of your choice at checkout. While this is obviously faster, it is also very secure.</p>
                                        </div>
                                        <div class="card-faq pb-3">
                                            <h6 class="h6">Is it safe to save my cards on Food Deliver?</h6>
                                            <p>Absolutely. Your card information is 100 percent safe with us. We use world class encryption technology while saving your card information on our highly secure systems. Our payment systems have passed stringent security compliance checks like PCI DSS (Payment Card Industry Data Security Standards) to ensure that your card information remains confidential at all times.</p>
                                        </div>
                                        <div class="card-faq pb-3">
                                            <h6 class="h6">What all card information does Food Deliver store?</h6>
                                            <p>Food Deliver only stores card information when the customer selects the option. We only store your card number, cardholder name and card expiry date. We do not store your card’s CVV number or the 3D Secure password.</p>
                                        </div>
                                        <div class="card-faq pb-3">
                                            <h6 class="h6">Can I delete my saved cards?</h6>
                                            <p>Yes, you can delete your saved cards at any given time.</p>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->
                            </div>
                            <!--/ right profile -->
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row-->                    
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->

    
    <!-- Modal -->
<div class="modal right fade" id="addnewcard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-5">

      <!-- form -->
      <form>
          <div class="form-group">
              <label>Card Number</label>
              <input type="text" placeholder="Enter Card Number" class="form-control">
          </div>

          <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                    <label>Expiry</label>
                    <select class="form-control">
                        <option>Month</option>
                        <option>January</option>
                        <option>February</option>
                        <option>March</option>
                        <option>April</option>
                        <option>May</option>
                        <option>Juner</option>
                        <option>July</option>
                        <option>August</option>
                        <option>September</option>
                        <option>October</option>
                        <option>November</option>
                        <option>December</option>
                    </select>
                 </div>
            </div> 
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Expiry</label>
                    <select class="form-control">
                        <option>Date</option>
                        <option>1</option>
                        <option>2</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>11</option>
                    </select>
                 </div>
            </div>                         
          </div>
          
          <div class="form-group">
              <label>Name on Card</label>
              <input type="text" placeholder="Name of On Card" class="form-control">
          </div>
          <div class="form-group">
              <label>Name this Card for future use</label>
              <input type="text" placeholder="Name for Future Use" class="form-control">
          </div>
      </form>
      <!--/ form -->
       
       

      </div>
      <div class="modal-footer">
        <button type="button" class="greenlink" data-dismiss="modal">Close</button>
        <button type="button" class="greenlink">Save Card</button>
      </div>
    </div>
  </div>
</div>
<!-- / popup modal for variations in food or additional food ends-->


</body>

<?php include 'includes/footerscripts.php' ?>


</html>