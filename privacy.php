<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Online Food Delivery in Hyderabad </title>
    <?php include 'includes/styles.php'?>
</head>

<body class="subbody">
    <!-- header-->
    <?php include 'includes/header.php'?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- sub page -->
        <div class="subpage">
            <!-- brudcrumbs-->
            <div class="breadcrumb">
                <!-- container -->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <h1 class="h4 pagetitle">Privacy Policy</h1>
                        </div>
                        <div class="col-lg-6">
                            <nav class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                    <li class="breadcrumb-item active">Privacy Policy</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ brudcrumb-->
            <!-- sub page body -->
            <div class="subpage-body content-page">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                         <!-- col -->
                         <div class="col-lg-12">
                            
                            <h5 class="h5 py-2">Privacy Policy</h5>
                             <p>Effective Date: November 1, 2017</p>
                             <p>We know that your privacy is important to you, and we work hard to earn and keep your trust. We have created this Privacy Policy to let you know what Personal Information we collect when you use this Site, how we use and share the Personal Information we collect, and how we protect your privacy.</p>
                             <p>If you have any questions about this Privacy Policy or our privacy practices, please contact us by e-mail at inquiries@foodsby.com or by mail at Foodsby LLC 3001 Broadway St NE, STE 170, Minneapolis, MN 55413.</p>
                             <p>This Privacy Policy applies to the online collection of Personal Information via software and Internet services and is incorporated into any applicable user agreement and service terms and any general terms of use for the Site (collectively, “Services”). This Privacy Policy does not apply to information collected in any other way. The Services may contain links to sites maintained by others. This Privacy Policy does not reflect the privacy practices of those sites. By using the above Services, you consent to the terms of this Privacy Policy and to Foodsby’s processing of Personal Information and non-personal information for the purposes provided in the Privacy Policy. You further consent to reviewing the Privacy Policy from time to time to view any updates.</p>
                             
                             <h5 class="h5 py-2">This Privacy Policy May Change</h5>
                             <p>This Privacy Policy describes Foodsby’s current policies and practices with regard to the Personal Information we collect through this Site.</p>
                             <p>We are continually improving and adding to the features and functionality of this Site and the services we offer through this Site. As a result of these changes (or changes in the law), we may need to update or revise this Privacy Policy. Accordingly, we reserve the right to update or modify this Privacy Policy at any time, without prior notice, by posting the revised version of this Privacy Policy behind the link marked “Privacy Policy” at the bottom of each page of this Site. Your continued use of this Site after we have posted the revised Privacy Policy constitutes your agreement to be bound by the revised Privacy Policy. However, we will honor the terms that were in effect when we gathered data from you.</p>
                             <p>For your convenience, whenever this Privacy Policy is changed, we will alert you by posting a notice on our home page for sixty days. We will also update the “effective date” at the top of this page. If more than sixty days goes by between your visits to this Site, be sure you check the effective date to see if this Privacy Policy has been revised since your last visit.</p>
                             <p>You may access the current version of this Privacy Policy at any time by clicking the link marked “Privacy Policy” at the bottom of each page of this Site.</p>
                             
                             <h5 class="h5 py-2">WHAT PERSONAL INFORMATION DOES FOODSBY COLLECT AND HOW DO WE USE IT?</h5>
                             <p>We collect information about our users in three ways: directly from the user, from our web server logs, and through cookies. We use the information primarily to provide you with a personalized Internet experience that delivers the information, resources, and services that are most relevant and helpful to you. We don’t share any of the information you provide with others, unless we say so in this Privacy Policy, or when we believe in good faith that the law requires it or when you authorize us to (for example, to our restaurant partners from whom you may order and receive food delivery).</p>
                            
                             <p>When you register for our services or order products via our site, we ask you to provide some Personal Information, for example, your name, address, phone number, email address, payment card information, and/or certain additional categories of information resulting from use of our websites and services. Other than payment card information, we keep this information in a contact database for future reference, as needed. We do not store any payment card information. Only our payment card processor has access to that information. We may use your email address you provide to offer you products and services that we believe may be of interest to you, via our newsletter. If you don’t wish to receive such offers, you may opt out (unsubscribe) via the link provided in these newsletters.</p>
                           
                             <p>If you contact us for customer support, we may ask you to provide information about your computer or about the issues you are trying to resolve. This information is necessary to help us answer your questions. We may record your requests and our responses for quality control purposes.</p>
                           
                             <p>Foodsby may make websites available to you for chat rooms, forums, message boards, or news groups. Please remember that any information disclosed in these areas is public. You should exercise caution when disclosing Personal Information in these areas. Don’t disclose information in these public forums that might be considered confidential.</p>

                             <p>We may track information about your visit to the Site and store that information in web or mobile server logs, which are records of the activities on the Site. Our servers automatically capture and save the information electronically. Examples of the information we may collect include</p>

                             <p>The information we collect in web server logs helps us administer the Site, analyze its usage, protect the website and its content from inappropriate use, and improve the user’s experience.</p>

                             <h5 class="h5 py-2">Your Obligations</h5>
                             <p>In order to offer and provide a customized and personal service, we may use cookies to store and help track information about you. Cookies are simply small pieces of data that are sent to your browser from a web server and stored on your computer’s hard drive.</p>

                             <p>The use of cookies is relatively standard. Most browsers are initially set up to accept cookies. However, if you prefer, you can set your browser to either notify you when you receive a cookie or to refuse to accept cookies. You should understand that some features of many sites may not function properly if you don’t accept cookies. In general, we use cookies and other web technologies to:</p>

                             <p>In addition to the cookies we deliver to your computer or mobile or tablet device through our Site, certain third parties may deliver cookies to you for a variety of reasons. For example, we use Google Analytics, a web analytics tool that helps us understand how visitors engage with our Site. To learn more about Google Analytics, click here.</p>

                             <p>Other third parties may deliver cookies to your computer or mobile or tablet device for the purpose of tracking your online behaviors across nonaffiliated websites and delivering targeted advertisements either on our Site or on other websites.</p>
                             
                             <p>You have choices about the collection of information by third parties on our Site. For example, if you don’t want information about your visit to our Site sent to Google Analytics, you may download an Opt-out Browser Add-on by clicking here. Please note that the Add-on does not prevent information from being sent to us.</p>

                             <p>In addition, if you would like to opt-out of having interest-based information collected by certain entities during your visits to our Site or other websites, please click here. You will be directed to an industry-developed website that contains mechanisms for choosing whether each listed entity may collect and use data for online behavioral advertising purposes. It may be that some of the third parties that collect interest-based information on our Site do not participate in the industry-developed opt-out website, in which case the best way to avoid third-party tracking of your online behaviors may be through your browser settings and deletion of cookies.</p>

                             <h5 class="h5 py-2">Children’s Privacy</h5>
                             <p>Services are not structured nor intended for users under 18 years of age. Accordingly, Foodsby will not knowingly collect or use any Personal Information from persons that Foodsby knows to be under the age of 18.</p>

                             <h5 class="h5 py-2">HOW DOES FOODSBY PROTECT THE PERSONAL INFORMATION THAT IT COLLECTS?</h5>
                             <p>We have implemented certain appropriate security measures to help protect your Personal Information from accidental loss and from unauthorized access, use, or disclosure. For example, some of our websites are protected with Secure Sockets Layer (SSL) technology. Also, we store the information about you in a data center with restricted access and appropriate monitoring, and we use a variety of technical security measures to secure your data. In addition, we use intrusion detection and virus protection software. However, please note that we cannot guarantee that unauthorized persons will always be unable to defeat our security measures.</p>

                             <p>Also, please note that we may store and process your Personal Information in systems located outside of your home country. However, regardless of where storage and processing may occur, we take appropriate steps to ensure that your information is protected, consistent with the principles set forth under this Privacy Policy.</p>

                             <h5 class="h5 py-2">HOW MAY I CORRECT, AMEND, OR DELETE MY PERSONAL INFORMATION AND/OR UPDATE MY PREFERENCES?</h5>
                             <p>You may cancel your registration or update your preferences at any time. If you don’t want to receive information about our products or services, please update your account preferences (where available), check the appropriate box when registering, and/or utilize the “unsubscribe” mechanism within the communications that you receive from us.</p>
                             
                             <p>If you have any additional questions or concerns related to this Privacy Policy and/or our practices, please email us at inquiries@foodsby.com.</p>


                         </div>
                         <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ sub page body -->
        </div>
        <!--/ sub page-->
      
    </main>
    <!--/ main -->
    <!-- footer -->
    <?php include 'includes/footer.php' ?>
    <!--/footer -->
</body>
<?php include 'includes/footerscripts.php' ?>
</html>